package com.example.application.repository;

import com.example.application.entity.ActiveUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class ActiveUserRepositoryTest {

    @Autowired
    ActiveUserRepository activeUserRepository;

    @Test
    void changeActiveUser(){
        ActiveUser activeUser = new ActiveUser();
        activeUser.setUsername("kambing");
        activeUser.setEmailAddress("kambing@yahoo.com");
        activeUser.setPassword("kkk");
        activeUserRepository.save(activeUser);

        try{
            activeUserRepository.changeActiveUser("kambing","kambing@yahoo.com","kkk",
                    "kebo","kebo@yahoo.com","kkkk");
        }
        catch(Exception e){

        }
    }

    @Test
    void insertActiveUser(){
        ActiveUser activeUser = ActiveUser.builder()
                        .username("isa")
                        .emailAddress("isa@yahoo.com")
                        .password("888")
                        .build();
        activeUserRepository.save(activeUser);
    }

    @Test
    public void printAllActiveUser(){
        List<ActiveUser> activeUserList = activeUserRepository.findAll();

        System.out.print("active user list: ");
        activeUserList.forEach(aul->{
            System.out.print(aul.getEmailAddress()+" ");
        });
        System.out.println();
    }

    @Test
    void deleteActiveUser(){
        ActiveUser activeUser = ActiveUser.builder()
                        .emailAddress("kambingguling@yahoo.com")
                        .password("kambing123")
                        .username("kambingguling")
                        .build();
        activeUserRepository.save(activeUser);

        try{
            activeUserRepository.deleteActiveUser("kambingguling");
        }
        catch(Exception e){

        }
    }
}
