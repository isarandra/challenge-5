package com.example.application.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SeatServiceTest {
    @Autowired
    ISeatService seatService;

    @Test
    public void printKursiYangTersedia(){
        seatService.printKursiYangTersedia(15l,"A");
    }

    @Test
    public void pesantiket(){
        seatService.pesantiket(15l,"A3");
    }
}
