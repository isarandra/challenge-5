package com.example.application.service;

import com.example.application.entity.Film;
import com.example.application.entity.Schedule;
import com.example.application.repository.ScheduleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FilmServiceTest {

    @Autowired
    IFilmService filmService;

    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    ISeatService seatService;

    @Test
    void newFilm_alreadyRegistered(){
        Film film = new Film();
        film.setFilmName("Finding Dory");
        film.setSedangTayang(true);
        try{
            filmService.newFilm(film);
        }
        catch (Exception e){

        }
        Assertions.assertThrows(Exception.class, () -> filmService.newFilm(film));
    }

//    @Test
//    void changeFilmName(){
//        Film film = new Film();
//        film.setFilmName("Dora The Explorer");
//        film.setSedangTayang(true);
//        try{
//            filmService.newFilm(film);
//        }
//        catch (Exception e){
//
//        }
//        try{
//            filmService.changeFilmName("Dora The Explorer", "Dora The Mageran");
//        }
//        catch (Exception e){
//
//        }
//    }

    @Test
    void deleteFilm(){
        Film film = Film
                .builder()
                .filmName("Boruto")
                .sedangTayang(false)
                .build();
        try{
            filmService.newFilm(film);
        }
        catch (Exception e){

        }
        try{
            filmService.deleteFilm("Boruto");
        }
        catch (Exception e){

        }
    }

    @Test
    public void updateNameFilm(){
        Film film = Film
                .builder()
                .filmName("Doraemon")
                .build();
        try{
            filmService.newFilm(film);
        }
        catch (Exception e){

        }
        try{
            filmService.updateNameFilm("Doraemon","DoraDora");
        }
        catch (Exception e){

        }
    }

    @Test
    public void printFilmYangSedangTayang(){
//        Film film1 = Film
//                .builder()
//                .filmName("Cabe")
//                .sedangTayang(true)
//                .build();
//        try{
//            filmService.newFilm(film1);
//        }
//        catch (Exception e){
//
//        }
//        Film film2 = Film
//                .builder()
//                .filmName("gelas")
//                .sedangTayang(false)
//                .build();
//        try{
//            filmService.newFilm(film2);
//        }
//        catch (Exception e){

//        }
        try{
            filmService.printFilmYangSedangTayang();
        }
        catch (Exception e){

        }
    }

    @Test
    public void newFilm(){
        Film film2 = Film.builder().filmName("Nemo").sedangTayang(false).build();
        try{
            filmService.newFilm(film2);
        }
        catch (Exception e){

        }

    }


}
