package com.example.application.repository;

import com.example.application.entity.Seat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SeatRepository extends JpaRepository<Seat, String> {

    @Query(
            value = "select * from seat where schedule_id=:schedule_id and studio_name=:studio_name",
            nativeQuery = true
    )
    List<Seat> getAllAvailableSeatByScheduleIdAndStudioName(@Param("schedule_id") Long schedule_id,
                                                            @Param("studio_name") String studio_name);

    @Query(
            value = "delete from seat where schedule_id=:schedule_id and nomor_kursi=:nomor_kursi",
            nativeQuery = true
    )
    void deleteRowByScheduleIdAndNomorKursi(@Param("schedule_id") Long scheduleId,@Param("nomor_kursi") String nomor_kursi);
}
