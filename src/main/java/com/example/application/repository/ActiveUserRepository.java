package com.example.application.repository;

import com.example.application.entity.ActiveUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ActiveUserRepository extends JpaRepository<ActiveUser, Long> {

    @Query(value= "select * from active_user where username = ?1", nativeQuery = true)
    List<ActiveUser> findActiveUserByUsername(@Param("username") String username);

    @Query(value = "call change_active_user(:usernameOld,:emailAddressOld,:passwordOld," +
            ":usernameNew,:emailAddressNew,:passwordNew)",nativeQuery = true)
    void changeActiveUser(String usernameOld,String emailAddressOld,String passwordOld,
                          String usernameNew,String emailAddressNew,String passwordNew);

    @Query(value = "delete from active_user where username=:username",nativeQuery = true)
    void deleteActiveUser(@Param("username") String username);

//    @Query(value = "update active_user" +
//            " set username = :username_new," +
//            "email_address= :email_address_new," +
//            "password= :password_new" +
//            " where username = :username_old",nativeQuery = true)
//    void updateActiveUser(@Param("username_old") String username_old,@Param("username_new") String username_new,
//                          @Param("email_address_new") String email_address_new,
//                          @Param("password_new") String password_new);
}
