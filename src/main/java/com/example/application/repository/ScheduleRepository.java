package com.example.application.repository;

import com.example.application.entity.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

    @Query(value = "select * from schedule where film_film_code = :filmCode",nativeQuery = true)
    List<Schedule> findAllScheduleByFilmCode(@Param("filmCode") Long filmCode);
    @Query(value = "select * from schedule where schedule_id=:schedule_id",nativeQuery = true)
    Schedule findScheduleByScheduleId(@Param("schedule_id")Long schedule_id);

}
