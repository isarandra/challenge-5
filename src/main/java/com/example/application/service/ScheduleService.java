package com.example.application.service;

import com.example.application.entity.Schedule;
import com.example.application.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScheduleService implements IScheduleService{

    @Autowired
    ScheduleRepository scheduleRepository;

    @Override
    public void newSchedule(Schedule schedule) {
        scheduleRepository.save(schedule);
    }
}
