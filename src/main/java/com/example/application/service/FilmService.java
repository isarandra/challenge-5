package com.example.application.service;

import com.example.application.entity.Film;
import com.example.application.entity.Schedule;
import com.example.application.repository.FilmRepository;
import com.example.application.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FilmService implements IFilmService{

    @Autowired
    FilmRepository filmRepository;

    @Autowired
    ScheduleRepository scheduleRepository;

    @Override
    public void newFilm(Film film) throws Exception {
        List<Film> listFilm = filmRepository.findFilmByFilmName(film.getFilmName());
        if(listFilm.size()>0){
            throw new Exception("Film sudah ada");
        }
        filmRepository.save(film);
    }

    @Override
    public void updateNameFilm(String namaFilmLama, String namaFilmBaru) throws Exception {
        List<Film> filmList = filmRepository.findFilmByFilmName(namaFilmLama);
        if(filmList.size()==0){
            throw new Exception("Film tidak ditemukan");
        }
        filmRepository.changeFilmName(namaFilmLama,namaFilmBaru);
    }

//    @Override
//    public void changeFilmName(String filmNameOld, String filmNameNew) throws Exception {
//        filmRepository.changeFilmName(filmNameOld,filmNameNew);
//    }

    @Override
    public void deleteFilm(String filmName) throws Exception {
        List<Film> listFilm = filmRepository.findFilmByFilmName(filmName);
        if(listFilm.size()==0){
            throw new Exception("Film tidak ada");
        }
        filmRepository.deleteFilm(filmName);
    }

    @Override
    public void printFilmYangSedangTayang() throws Exception{
        List<Film> filmList = filmRepository.findAllFilmYangSedangTayang();
        if(filmList.size()==0){
            throw new Exception("Tidak ada film yang sedang tayang");
        }
        System.out.println("Film yang sedang tayang:");

        List<String> filmListUnique= new ArrayList<>();

        filmList.forEach(fl->{
//            System.out.println(fl.getFilmName());
            if(!filmListUnique.contains(fl.getFilmName())){
                filmListUnique.add(fl.getFilmName());
            }
        });
        filmListUnique.forEach(flu->{
            System.out.println(flu);
        });
        System.out.println();
    }
}
