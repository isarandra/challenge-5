package com.example.application.service;

import com.example.application.entity.Seat;
import com.example.application.repository.SeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SeatService implements ISeatService{

    @Autowired
    SeatRepository seatRepository;

    @Override
    public void printKursiYangTersedia(Long scheduleId,String studioName) {
        List<Seat> seatList=seatRepository.getAllAvailableSeatByScheduleIdAndStudioName(scheduleId,studioName);
        System.out.println("Kursi yang tersedia:");
        seatList.forEach(
                x->{
                    System.out.println(x.getNomorKursi());
                }
        );
        System.out.println();
    }

    @Override
    public void pesantiket(Long scheduleId,String nomorKursi) {
        try{
            seatRepository.deleteRowByScheduleIdAndNomorKursi(scheduleId,nomorKursi);
        }
        catch (Exception e){

        }
    }
}

