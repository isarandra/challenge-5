package com.example.application.service;

import org.springframework.stereotype.Service;

@Service
public interface ISeatService {

    void printKursiYangTersedia(Long scheduleId,String studioName);

    void pesantiket(Long scheduleId, String nomorKursi);
}
