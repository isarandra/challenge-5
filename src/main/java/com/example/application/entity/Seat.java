package com.example.application.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@IdClass(SeatId.class)
public class Seat {

    @Id
    private String studioName;

    @Id
    private String nomorKursi;

    @Id
    private Long scheduleId;
}
