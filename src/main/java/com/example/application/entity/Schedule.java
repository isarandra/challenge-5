package com.example.application.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Schedule {

    @Id
    @SequenceGenerator(
            name = "schedule_id_sequence",
            sequenceName = "schedule_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "schedule_id_sequence"
    )
    private Long scheduleId;

    private String tanggalTayang;

    private String jamMulai;

    private String jamSelesai;

    private Long hargaTiket;

    private String studioName;



    @ManyToOne(
            cascade = CascadeType.ALL
    )
    @JoinColumn(
//            name = "film_code",
            referencedColumnName = "filmCode"
    )
    private Film film;
}
